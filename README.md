# README #

Textual action column for Yii2

![actioncolumn.jpg](https://bitbucket.org/repo/5p75jp/images/1524899438-actioncolumn.jpg)

### How do I get set up? ###

Add to composer:

```
#!js
"require": {
  "azbuco/yii2-actioncolumn": "dev-master"
}
"repositories": [
  {
    "type": "vcs",
    "url": "https://azbuco@bitbucket.org/azbuco/yii2-actioncolumn.git"
  }
```

Use in a yii gridview config like this:

```
#!php

'columns' => [
  [
    'class' => azbuco\actioncolumn\ActionColumn::className(),
    'attribute' => 'name',
  ],
```

Manage buttons like \yii\grid\ActionColumn, manage content like \yii\grid\DataColumn