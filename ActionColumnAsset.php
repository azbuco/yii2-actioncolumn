<?php

namespace azbuco\actioncolumn;

use yii\web\AssetBundle;

class ActionColumnAsset extends AssetBundle
{
    public $sourcePath = '@azbuco/actioncolumn/assets';
    public $css = [
        'css/actioncolumn.css',
    ];
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

}
